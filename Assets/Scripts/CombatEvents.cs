﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatEvents : MonoBehaviour
{
    public delegate void UnitEventHandler(UnitInterface unit);
    public static UnitEventHandler onUnitDeath;

    public static void EnemyDied(UnitInterface unit)
    {
        if (onUnitDeath != null)
            onUnitDeath(unit);
    }
}
