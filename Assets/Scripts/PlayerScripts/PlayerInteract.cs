﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class PlayerInteract : MonoBehaviour
{

    NavMeshAgent navAgent;
    public GameObject target = null;
    public Inventory inventory;
    private bool targetInDistance = false;
    UnitHealth unitHealthScripts;
    MouseCursor mouseCursorScripts;
    PlayerAnimations playerAnimationsScripts;
    LookAt lookAtScripts;
    PlayerGold playerGoldScripts;
    // Use this for initialization
    void Start()
    {
        playerGoldScripts = GetComponent<PlayerGold>();
        navAgent = GetComponent<NavMeshAgent>();
        playerAnimationsScripts = GetComponent<PlayerAnimations>();
        lookAtScripts = GetComponent<LookAt>();
        mouseCursorScripts = GetComponent<MouseCursor>();
        unitHealthScripts = GetComponent<UnitHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            MoveToTarget();
            CheckDistance();
            if (targetInDistance)
            {
                lookAtScripts.LookAtThe(target);
                if (Input.GetMouseButtonDown(1) && !EventSystem.current.IsPointerOverGameObject())
                {
                    if (target.gameObject.tag == "Enemy")
                    {
                        StartCoroutine("Ready");
                        playerAnimationsScripts.StartEffect();
                    }
                }
                if (target.gameObject.tag == "Item")
                {
                    target.GetComponent<Item>().kokoHealth = unitHealthScripts;
                    inventory.AddItem(target.GetComponent<Item>());
                    Destroy(target);
                }
                if (target.gameObject.tag == "Gold")
                {
                    int money = target.GetComponent<GoldValue>().goldValue;
                    playerGoldScripts.GetGold(money);
                    Destroy(target);
                }
            }
        }
        else
        {
            target = null;
        }
    }

    public void MoveToTarget()
    {
        mouseCursorScripts.CursorSwitch(false);
        navAgent.SetDestination(target.transform.position);
    }

    void CheckDistance()
    {
        float distance = Vector3.Distance(target.transform.position, transform.position);
        Vector3 dir = (target.transform.position - transform.position);
        float direction = Vector3.Dot(dir, transform.forward);
        if (distance < 2.5f && direction > 0)
        {
            targetInDistance = true;
            navAgent.ResetPath();
        }
        else
        {
            targetInDistance = false;
        }
    }

    IEnumerator Ready()
    {
        playerAnimationsScripts.AttackStanceAnimation();
        yield return new WaitForSeconds(2);
    }
}
