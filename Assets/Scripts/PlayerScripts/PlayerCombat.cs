﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerCombat : MonoBehaviour
{
    LookAt lookAtScripts;
    NavMeshAgent navAgent;
    public GameObject target;
    PlayerAnimations playerAnimationsScripts;
    private bool targetInDistance = false;
    UnitHealth unitHealthScripts;
    MouseCursor mouseCursorScripts;
    // Use this for initialization
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        playerAnimationsScripts = GetComponent<PlayerAnimations>();
        lookAtScripts = GetComponent<LookAt>();
        mouseCursorScripts = GetComponent<MouseCursor>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            MoveToTarget();
            CheckDistance();
            if (targetInDistance)
            {
                lookAtScripts.LookAtThe(target);
                playerAnimationsScripts.AttackStanceAnimation();
                if (Input.GetMouseButtonDown(1))
                {
                    playerAnimationsScripts.StartEffect();
                }
            }
        }
        else
        {
            target = null;
        }
    }

    public void MoveToTarget()
    {
        mouseCursorScripts.CursorSwitch(false);
        navAgent.SetDestination(target.transform.position);
    }

    void CheckDistance()
    {
        float distance = Vector3.Distance(target.transform.position, transform.position);
        Vector3 dir = (target.transform.position - transform.position);
        float direction = Vector3.Dot(dir, transform.forward);
        if (distance < 2.5f && direction > 0)
        {
            targetInDistance = true;
            navAgent.ResetPath();
        }
        else
        {
            targetInDistance = false;
        }
    }
}
