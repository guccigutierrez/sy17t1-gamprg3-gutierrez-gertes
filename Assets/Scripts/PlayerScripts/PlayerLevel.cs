﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevel : MonoBehaviour
{
    public int level;
    public int XPPerLevel
    {
        get
        {
            return level * 50;
        }
    }
    public int currentXP
    {
        get;
        set;
    }
    PlayerAddPoints playerAddPointsScripts;
    public int seeCurXP;
    public int seeReqXP;
    // Use this for initialization
    void Start()
    {
        level = 1;
        currentXP = 0;
        playerAddPointsScripts = GetComponent<PlayerAddPoints>();
        CombatEvents.onUnitDeath += EnemyToExperience;
        UIEventHandler.PlayerLeveled();
    }

    // Update is called once per frame
    void Update()
    {
        seeCurXP = currentXP;
        seeReqXP = XPPerLevel;
    }

    public void EnemyToExperience(UnitInterface enemy)
    {
        GrantExperience(enemy.Experience);
    }
    public void GrantExperience(int amount)
    {
        currentXP += amount;
        while (currentXP >= XPPerLevel)
        {
            playerAddPointsScripts.AddAllocationPoints();
            currentXP -= XPPerLevel;
            level++;
        }
        UIEventHandler.PlayerLeveled();
    }
}
