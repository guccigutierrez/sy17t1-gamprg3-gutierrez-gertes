﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    NavMeshAgent navAgent;
    MouseCursor mouseCursorScripts;
    PlayerInteract playerCombatScripts;
    PlayerAnimations playerAnimationsScripts;
    // Use this for initialization
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        mouseCursorScripts = GetComponent<MouseCursor>();
        playerCombatScripts = GetComponent<PlayerInteract>();
        playerAnimationsScripts = GetComponent<PlayerAnimations>();
    }

    void Update()
    {
        float distance = navAgent.remainingDistance;
        if (distance != Mathf.Infinity && navAgent.pathStatus == NavMeshPathStatus.PathComplete && navAgent.remainingDistance == 0)
        {
            playerAnimationsScripts.IdleAnimation();
            mouseCursorScripts.CursorSwitch(false);
        }
        else
        {
            playerAnimationsScripts.RunningAnimation();
            mouseCursorScripts.CursorSwitch(true);
        }
    }

    public void MoveToPoint(Vector3 point)
    {
        playerCombatScripts.target = null;
        navAgent.SetDestination(point);
    }
}
