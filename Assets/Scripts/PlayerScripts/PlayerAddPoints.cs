﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAddPoints : MonoBehaviour
{
    public int allocationPoints = 0;
    UnitStats addPointsTo;
    // Use this for initialization
    void Start()
    {
        addPointsTo = GetComponent<UnitStats>();
        UIEventHandler.PlayerDetails();
    }

    public void AddAllocationPoints()
    {
        allocationPoints++;
    }
    void DeleteAllocationPoints()
    {
        allocationPoints--;
    }
    public void AddPointsSTR()
    {
        if (allocationPoints > 0)
        {
            addPointsTo.strength++;
            DeleteAllocationPoints();
        }
    }
    public void AddPointsVIT()
    {
        if (allocationPoints > 0)
        {
            addPointsTo.vitality++;
            DeleteAllocationPoints();
        }
    }
}
