﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    Animator animator;
    UnitHealth unitHealthScripts;
    UnitStats unitStatsScripts;
    PlayerInteract playerCombatScripts;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        unitStatsScripts = GetComponent<UnitStats>();
        playerCombatScripts = GetComponent<PlayerInteract>();
    }

    public void IdleAnimation()
    {
        animator.SetBool("isRunning", false);
    }
    public void SheathBlade()
    {
        animator.SetBool("isAttacking", false);
    }
    public void RunningAnimation()
    {
        animator.SetBool("isRunning", true);
    }
    public void AttackStanceAnimation()
    {
        animator.SetBool("isRunning", false);
        animator.SetBool("isAttacking", true);
    }

    public void StartEffect()
    {
        animator.SetBool("ExecuteAttack", true);
    }
    void DamageTo()
    {
        unitHealthScripts = playerCombatScripts.target.transform.gameObject.transform.GetComponent<UnitHealth>();
        unitHealthScripts.RecieveDamage(unitStatsScripts.AttackPower, playerCombatScripts.target);
    }
    void StopEffect()
    {
        animator.SetBool("ExecuteAttack", false);
    }
}
