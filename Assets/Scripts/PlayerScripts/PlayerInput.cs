﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour
{
    public LayerMask movement;
    public LayerMask interactables;
    Ray interactionRay;
    UnitHealth unitHealthScripts;
    RaycastHit interactionInfo;
    MouseCursor mouseCursorScript;
    PlayerInteract interact;
    PlayerMovement playerMovementScripts;

    // Use this for initialization
    void Start()
    {
        unitHealthScripts = GetComponent<UnitHealth>();
        playerMovementScripts = GetComponent<PlayerMovement>();
        mouseCursorScript = GetComponent<MouseCursor>();
        interact = GetComponent<PlayerInteract>();
    }

    // Update is called once per frame
    void Update()
    {
        if (unitHealthScripts.alive)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (Input.GetMouseButtonDown(0))
                {
                    MovePlayer();
                }
                if (Input.GetMouseButtonDown(1))
                {
                    InteractObject();
                }
            }
        }
    }
    void MovePlayer()
    {
        interactionRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity, movement))
        {
            playerMovementScripts.MoveToPoint(interactionInfo.point);
            mouseCursorScript.MoveCursor(interactionInfo.point);
        }

    }

    void InteractObject()
    {
        interactionRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity, interactables))
        {
            interact.target = interactionInfo.transform.gameObject;
        }
    }
}
