﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour
{
    public GameObject pointer;
    private Vector3 positionCursor;
    PlayerInteract playerCombatScripts;
    public void MoveCursor(Vector3 raycastPoint)
    {
        positionCursor = new Vector3(raycastPoint.x, raycastPoint.y + 0.5f, raycastPoint.z);
        pointer.transform.position = positionCursor;
    }

    public void CursorSwitch(bool switcher)
    {
        pointer.gameObject.SetActive(switcher);
    }
}
