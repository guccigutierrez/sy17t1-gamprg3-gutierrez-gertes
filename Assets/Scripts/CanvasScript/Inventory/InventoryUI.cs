﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : Inventory
{
    public GameObject slotPrefab;
    private int rows = 4;
    private int column = 5;
    // Use this for initialization
    void Awake()
    {
        CreateSlots();
    }

    void CreateSlots()
    {
        Inventory.AllSlots = new List<GameObject>();
        EmptySlots = 20;
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < column; x++)
            {
                GameObject newSlot = Instantiate(slotPrefab);
                newSlot.name = "Slot";
                newSlot.transform.parent = gameObject.transform;
                Inventory.AllSlots.Add(newSlot);
            }
        }
    }
}
