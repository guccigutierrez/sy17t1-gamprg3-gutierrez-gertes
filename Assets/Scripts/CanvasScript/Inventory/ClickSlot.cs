﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickSlot : MonoBehaviour, IPointerClickHandler
{
    Slot slotScripts;
    // Use this for initialization
    void Start()
    {
        slotScripts = GetComponent<Slot>();
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right && EventSystem.current.IsPointerOverGameObject())
        {
            slotScripts.UseItem();
        }
    }
}
