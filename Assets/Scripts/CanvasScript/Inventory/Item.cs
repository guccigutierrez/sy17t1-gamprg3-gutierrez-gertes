﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public string itemName;
    public int itemID;
    public string itemDescription;
    public Sprite spriteNeutral;
    public Sprite spriteHighlighted;
    public int itemRestorePoints;
    public int maximumSize;
    public ItemType itemType;
    public Quality quality;
    public UnitHealth kokoHealth;

    public enum ItemType
    {
        Weapon,
        Consumable,
    }
    public enum Quality
    {
        Common,
        Uncommon,
        Rare,
        Epic,
        Legendary,
        Artifact
    }

    public void Use()
    {
        switch (itemType)
        {
            case ItemType.Consumable:
                Debug.Log("I use an item!!!");
                kokoHealth.currentHealth += itemRestorePoints;
                break;
            case ItemType.Weapon:
                break;
        }
    }

    public string GetTooltip()
    {
        string stats = string.Empty;
        string color = string.Empty;
        string newLine = string.Empty;
        string result = string.Empty;

        if (itemDescription != string.Empty)
        {
            newLine = "\n";
        }
        switch (quality)
        {
            case Quality.Common:
                color = "white";
                break;
            case Quality.Uncommon:
                color = "lime";
                break;
            case Quality.Rare:
                color = "navy";
                break;
            case Quality.Epic:
                color = "magenta";
                break;
            case Quality.Legendary:
                color = "orange";
                break;
            case Quality.Artifact:
                color = "red";
                break;
        }

        if (itemRestorePoints > 0)
        {
            stats += "\n+" + itemRestorePoints.ToString() + " Health Points";
        }
        result = string.Format("<color=" + color + "><size=14>{0}</size></color><size=12><i><color=lime>" + newLine + "{1}</color></i>{2}</size>", itemName, itemDescription, stats);
        return result;
    }
}