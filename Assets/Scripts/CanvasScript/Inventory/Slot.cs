﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    private Stack<Item> items;
    public Text stackText;
    private ChangeSprite changeSpriteScripts;
    public bool IsEmpty
    {
        get
        {
            return items.Count == 0;
        }
    }

    public Item CurrentItem
    {
        get
        {
            return items.Peek();
        }
    }

    public bool isAvailable
    {
        get
        {
            return CurrentItem.maximumSize > items.Count;
        }
    }
    // Use this for initialization
    void Start()
    {
        items = new Stack<Item>();
        changeSpriteScripts = GetComponent<ChangeSprite>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddItem(Item item)
    {
        items.Push(item);
        if (items.Count > 1)
        {
            stackText.text = items.Count.ToString();
        }
        changeSpriteScripts.SwapSprite(item.spriteNeutral, item.spriteHighlighted);
    }

    public void UseItem()
    {
        if(!IsEmpty)
        {
            items.Pop().Use();
            stackText.text = items.Count > 1 ? items.Count.ToString() : string.Empty;
            if(IsEmpty)
            {
                changeSpriteScripts.SwapSprite(changeSpriteScripts.slotEmpty, changeSpriteScripts.slotHightlight);
                Inventory.EmptySlots++;
            }
        }
    }
}
