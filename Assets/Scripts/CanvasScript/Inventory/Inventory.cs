﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    static List<GameObject> allSlots;
    private static int emptySlots;
    public static int EmptySlots
    {
        get
        {
            return emptySlots;
        }

        set
        {
            emptySlots = value;
        }
    }
    public static List<GameObject> AllSlots
    {
        get
        {
            return allSlots;
        }

        set
        {
            allSlots = value;
        }
    }
    public bool PlaceEmpty(Item item)
    {
        if (EmptySlots > 0)
        {
            foreach (GameObject slot in AllSlots)
            {
                Slot temporary = slot.GetComponent<Slot>();
                if(temporary.IsEmpty)
                {
                    temporary.AddItem(item);
                    EmptySlots--;
                    return true;
                }
            }
        }
        return false;
    }

    public bool AddItem(Item item)
    {
        if (item.maximumSize == 1)
        {
            PlaceEmpty(item);
            return true;
        }
        else
        {
            foreach(GameObject slot in AllSlots)
            {
                Slot temporary = slot.GetComponent<Slot>();
                if(!temporary.IsEmpty)
                {
                    if(temporary.CurrentItem.itemID == item.itemID && temporary.isAvailable)
                    {
                        temporary.AddItem(item);
                        EmptySlots--;
                        return true;
                    }
                }
            }
            if(EmptySlots > 0)
            {
                PlaceEmpty(item);
            }
        }
        return false;
    }
}
