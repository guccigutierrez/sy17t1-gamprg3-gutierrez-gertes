﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTooltip : MonoBehaviour
{
    public enum Quality
    {
        Common,
        Uncommon,
        Rare,
        Epic,
        Legendary,
        Artifact
    }
    public Quality quality;
    Item item;
    // Use this for initialization
    void Start()
    {
        item = GetComponent<Item>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public string GetTooltip()
    {
        string stats = string.Empty;
        string color = string.Empty;
        string newLine = string.Empty;
        string result = string.Empty;

        if(item.itemDescription != string.Empty)
        {
            newLine = "\n";
        }
        switch(quality)
        {
            case Quality.Common:
                color = "white";
                break;
            case Quality.Uncommon:
                color = "lime";
                break;
            case Quality.Rare:
                color = "navy";
                break;
            case Quality.Epic:
                color = "magenta";
                break;
            case Quality.Legendary:
                color = "orange";
                break;
            case Quality.Artifact:
                color = "red";
                break;
        }

        if(item.itemRestorePoints > 0)
        {
            stats += "\n+" + item.itemRestorePoints.ToString() + "Health Points";
        }
        result = string.Format("<color=" + color + "><size=16>{0}</size></color><size=14><i><color=lime>"+newLine+"{1}</color></i>{2}</size>", item.itemName, item.itemDescription, stats);
        return result;
    }
}
