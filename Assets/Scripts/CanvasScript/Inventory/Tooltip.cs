﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    public GameObject tooltipObject;
    private static GameObject tooltip;
    public Text sizeTextObject;
    public static Text sizeText;
    public Text visualTextObject;
    public static Text visualText;
    // Use this for initialization
    void Start()
    {
        tooltip = tooltipObject;
        sizeText = sizeTextObject;
        visualText = visualTextObject;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowTooltip(GameObject slot)
    {
        Slot temporarySlot = slot.GetComponent<Slot>();
        if(!temporarySlot.IsEmpty)
        {
            visualText.text = temporarySlot.CurrentItem.GetTooltip();
            sizeText.text = visualText.text;
            tooltip.SetActive(true);
            float xPosition = slot.transform.position.x;
            float yPostion = slot.transform.position.y - slot.GetComponent<RectTransform>().sizeDelta.y;
            tooltip.transform.position = new Vector2(xPosition, yPostion);
        }
        
    }

    public void HideTooltip()
    {
        tooltip.SetActive(false);
    }
}
