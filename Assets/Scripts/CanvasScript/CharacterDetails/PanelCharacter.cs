﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelCharacter : MonoBehaviour
{
    [SerializeField]private Text target, health, level, exp, attack, statPoints;
    [Header("Character Stats")]
    public Text STR;
    public Text VIT;
    [Header("Scripts")]
    [SerializeField] UnitStats unitStatsScripts;
    [SerializeField] PlayerAddPoints playerAddPointsScripts;
    [SerializeField] PlayerLevel playerLevelScripts;
    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(false);
    }
    void Awake()
    {
        UIEventHandler.onPlayerHealthChanged += UpdateHealth;
        UIEventHandler.OnPlayerLevel += UpdateLevel;
        UIEventHandler.OnPlayerDetails += UpdateDetails;
        UIEventHandler.OnStatsChanged += UpdateStats;
    }

    // Update is called once per frame
    void UpdateHealth(int currentHealth, int maximumHealth)
    {
        health.text = currentHealth.ToString();
    }

    void UpdateLevel()
    {
        level.text = playerLevelScripts.level.ToString();
        exp.text = "EXP: " + playerLevelScripts.currentXP.ToString();
    }

    void UpdateDetails()
    {
        attack.text = "Attack: " + unitStatsScripts.AttackPower.ToString();
        statPoints.text = "Stat Points: " + playerAddPointsScripts.allocationPoints.ToString();
        target.text = "Name: " + playerLevelScripts.gameObject.name;
    }

    void UpdateStats()
    {
        STR.text = "Strength: " + unitStatsScripts.strength.ToString();
        VIT.text = "Vitality: " + unitStatsScripts.vitality.ToString();
    }
}
