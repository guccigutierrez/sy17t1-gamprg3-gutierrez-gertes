﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public GameObject characterPanel;
    public GameObject inventoryPanel;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("characterPanel"))
        {
            characterPanel.SetActive(!characterPanel.activeSelf);
        }
        if (Input.GetButtonDown("Inventory"))
        {
            inventoryPanel.SetActive(!inventoryPanel.activeSelf);
        }
    }
}
