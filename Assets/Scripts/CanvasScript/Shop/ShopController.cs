﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : MonoBehaviour
{
    public GameObject linkShopInventory;
    private bool inRange = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(inRange)
        {
            if (Input.GetButtonDown("ShopInventory"))
            {
                linkShopInventory.SetActive(!linkShopInventory.activeSelf);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            inRange = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            inRange = false;
            linkShopInventory.SetActive(false);
        }
    }
}
