﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopInventory : MonoBehaviour
{
    public Inventory inventory;
    public GameObject player;
    public List<Item> SellingItems = new List<Item>();
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void GiveItem()
    {

    }
    public void BuyItem(Item item)
    {
        //player.GetComponent<PlayerGold>().currentGold -= item.goldCost;
        inventory.AddItem(item.GetComponent<Item>());
    }
}
