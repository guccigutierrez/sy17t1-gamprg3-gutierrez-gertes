﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : ShopInventory
{
    public GameObject slotPrefab;
    private int rows = 6;
    private int column = 8;
    // Use this for initialization
    void Start()
    {
        CreateSlots();
    }

    void CreateSlots()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < column; x++)
            {
                GameObject newSlot = Instantiate(slotPrefab);
                newSlot.name = "Slot";
                newSlot.transform.parent = gameObject.transform;
            }
        }
    }
}
