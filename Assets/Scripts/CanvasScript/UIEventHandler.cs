﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventHandler : MonoBehaviour
{
    public delegate void PlayerHealthEventHandler(int currentHealth, int maximumHealth);
    public static event PlayerHealthEventHandler onPlayerHealthChanged;

    public delegate void StatsEventHandler();
    public static event StatsEventHandler OnStatsChanged;

    public delegate void PlayerLevelEventHandler();
    public static event PlayerLevelEventHandler OnPlayerLevel;

    public delegate void PlayerDetailsEventHandler();
    public static event PlayerDetailsEventHandler OnPlayerDetails;

    public static void HealthChanged(int currentHealth, int maximumHealth)
    {
        onPlayerHealthChanged(currentHealth, maximumHealth);
    }

    public static void StatsChanged()
    {
        OnStatsChanged();
    }

    public static void PlayerLeveled()
    {
        OnPlayerLevel();
    }

    public static void PlayerDetails()
    {
        OnPlayerDetails();
    }
}
