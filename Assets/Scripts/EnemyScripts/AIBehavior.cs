﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBehavior : MonoBehaviour
{
    public enum State
    {
        Chase,
        Patrol,
        Attack
    }
    LookAt lookAtScripts;
    public int attackSpeed;
    NavMeshAgent navAgent;
    UnitHealth unitHealthScripts;
    AIPatrol AIPatrolScripts;
    public GameObject target;
    public State CurrentState;
    EnemyAnimation EnemyAnimationScripts;
    // Use this for initialization
    void Start()
    {
        CurrentState = State.Patrol;
        navAgent = GetComponent<NavMeshAgent>();
        unitHealthScripts = GetComponent<UnitHealth>();
        lookAtScripts = GetComponent<LookAt>();
        AIPatrolScripts = GetComponent<AIPatrol>();
        EnemyAnimationScripts = GetComponent<EnemyAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (unitHealthScripts.alive)
        {
            if (CurrentState == State.Attack)
            {
                Attack();
            }
            else if (CurrentState == State.Chase)
            {
                Chase();
            }
            else if (CurrentState == State.Patrol)
            {
                Patrol();
            }
        }
    }
    void Attack()
    {
        if (target != null)
        {
            CheckDistance();
            lookAtScripts.LookAtThe(target);
            EnemyAnimationScripts.IdleAnimation();
            EnemyAnimationScripts.StartAttack();
        }
        else
        {
            target = null;
            CurrentState = State.Patrol;
        }
    }

    void Chase()
    {
        if (target != null)
        {
            EnemyAnimationScripts.RunAnimation();
            navAgent.SetDestination(target.transform.position);
            CheckDistance();
        }
    }
    void Patrol()
    {
        EnemyAnimationScripts.WalkAnimation();
        AIPatrolScripts.EnemyWaypoints();
    }

    void CheckDistance()
    {
        float distance = Vector3.Distance(target.transform.position, transform.position);
        Vector3 dir = (target.transform.position - transform.position);
        float direction = Vector3.Dot(dir, transform.forward);
        if (distance < 2 && direction > 0)
        {
            CurrentState = State.Attack;
            navAgent.ResetPath();
        }
        if (distance < 8 && direction > 2)
        {
            CurrentState = State.Chase;
        }
        if (distance > 10 && direction > 8)
        {
            target = null;
            CurrentState = State.Patrol;
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            CurrentState = State.Chase;
            target = collision.gameObject;
        }
    }
}
