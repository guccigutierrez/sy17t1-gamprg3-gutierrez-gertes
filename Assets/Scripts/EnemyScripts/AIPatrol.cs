﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIPatrol : MonoBehaviour
{
    NavMeshAgent navAgent;
    public GameObject[] waypoints;
    public int waypointIndex = 0;
    EnemyAnimation EnemyAnimationScripts;
    // Use this for initialization
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        waypointIndex = Random.Range(0, waypoints.Length);
        EnemyAnimationScripts = GetComponent<EnemyAnimation>();
    }

    public void EnemyWaypoints()
    {
        float distance = navAgent.remainingDistance;
        if (distance != Mathf.Infinity && navAgent.pathStatus == NavMeshPathStatus.PathComplete && navAgent.remainingDistance == 0)
        {
            StartCoroutine("Rest");
        }      
    }

    IEnumerator Rest()
    {
        waypointIndex = Random.Range(0, waypoints.Length);
        EnemyAnimationScripts.IdleAnimation();
        yield return new WaitForSeconds(2);
        navAgent.SetDestination(waypoints[waypointIndex].transform.position);
    }
}
