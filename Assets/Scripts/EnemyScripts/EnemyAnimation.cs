﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    Animator animator;
    UnitHealth unitHealthScripts;
    UnitStats unitStatsScripts;
    AIBehavior AIBehaviorScripts;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        unitStatsScripts = GetComponent<UnitStats>();
        AIBehaviorScripts = GetComponent<AIBehavior>();
    }

    public void IdleAnimation()
    {
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isAttacking", false);
    }
    public void RunAnimation()
    {
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", true);
        animator.SetBool("isAttacking", false);
    }
    public void WalkAnimation()
    {
        animator.SetBool("isWalking",true);
        animator.SetBool("isRunning", false);
        animator.SetBool("isAttacking", false);
    }

    public void StartAttack()
    {
        animator.SetBool("isAttacking", true);
    }
    void ApplyDamage()
    {
        unitHealthScripts = AIBehaviorScripts.target.transform.gameObject.transform.GetComponent<UnitHealth>();
        unitHealthScripts.RecieveDamage(unitStatsScripts.AttackPower, AIBehaviorScripts.target);
    }
    void EndAttack()
    {
        animator.SetBool("isAttacking", false);
    }
}
