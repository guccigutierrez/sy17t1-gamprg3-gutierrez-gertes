﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropLoot : LootTable
{
    UnitHealth deadLoot;
    public int dropChance;
    public void CalculateLoot()
    {
        int calculatedDropChange = Random.Range(0, 101);
        if (calculatedDropChange > dropChance)
        {
            Debug.Log("No loot for you!");
            return;
        }
        if (calculatedDropChange <= dropChance)
        {
            int itemWeight = 0;
            for (int i = 0; i < LootDatabase.Count; i++)
            {
                itemWeight += LootDatabase[i].rarity;
            }
            Debug.Log("itemWeight = " + itemWeight);
            int randomValue = Random.Range(0, itemWeight);
            for (int j = 0; j < LootDatabase.Count; j++)
            {
                if (randomValue <= LootDatabase[j].rarity)
                {
                    GameObject newObject = Instantiate(LootDatabase[j].itemPrefab, transform.position, gameObject.transform.rotation);
                    return;
                }
                randomValue -= LootDatabase[j].rarity;
            }
        }
    }
}
