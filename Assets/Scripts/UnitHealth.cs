﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHealth : MonoBehaviour, UnitInterface
{
    public int currentHealth;
    public int maximumHealth;
    UnitStats unitStatsScripts;
    DropLoot dropLootScripts;
    public int experienceGiven;
    public int Experience
    {
        get { return experienceGiven; }
        set { experienceGiven = value; }
    }

    public bool alive;

    void Awake()
    {
        dropLootScripts = GetComponent<DropLoot>();
        unitStatsScripts = GetComponent<UnitStats>();
        maximumHealth = unitStatsScripts.Health;
        currentHealth = maximumHealth;   
    }
    void Update()
    {
        UpdatePlayerPanel();
        if(currentHealth > maximumHealth)
        {
            currentHealth = maximumHealth;
        }
    }

    public void RecieveDamage(int damage, GameObject target)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            OnDeath();
            print(target.name + " has died");
        }

    }
    public void OnDeath()
    {
        alive = false;
        dropLootScripts.CalculateLoot();
        Destroy(gameObject);
        CombatEvents.EnemyDied(this);
    }

    void UpdatePlayerPanel()
    {
        if (gameObject.tag == "Player")
        {
            UIEventHandler.HealthChanged(this.currentHealth, this.maximumHealth);
        }
    }
}
