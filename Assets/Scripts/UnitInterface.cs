﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface UnitInterface
{
    int Experience
    {
        get;
        set;
    }
    void OnDeath();
}
