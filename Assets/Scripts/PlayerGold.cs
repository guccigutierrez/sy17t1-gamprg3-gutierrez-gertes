﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGold : MonoBehaviour
{
    public int currentGold;
    public void GetGold(int goldValue)
    {
        currentGold += goldValue;
    }
}
