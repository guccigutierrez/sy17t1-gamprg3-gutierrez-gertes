﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTable : MonoBehaviour
{
    [System.Serializable]
    public class DropRarity
    {
        public string dropableItemName;
        public GameObject itemPrefab;
        public int rarity;
    }
    public List<DropRarity> LootDatabase = new List<DropRarity>();
}
