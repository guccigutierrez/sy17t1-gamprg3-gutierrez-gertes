﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStats : MonoBehaviour
{
    public int strength;
    public int vitality;

    public int AttackPower
    {
        get
        {
            return strength * 2;
        }
    }
    public int Health
    {
        get
        {
            return vitality * 10;
        }
    }

    void Update()
    {
        PlayerDetails();
    }

    void PlayerDetails()
    {
        if(gameObject.tag == "Player")
        {
            UIEventHandler.PlayerDetails();
            UIEventHandler.StatsChanged();
        }
    }
}
